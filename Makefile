#
# Install dotfile.
#

PREFIX	= dot

define symlink
	@if [ -L "$(HOME)/$(1)" ]; then \
		rm "$(HOME)/$(1)"; \
	fi
	@if [ ! -f "$(HOME)/$(1)" ]; then \
		ln -sf $(CURDIR)/$(PREFIX)$(1) $(HOME)/$(1); \
	fi
endef

usage:
	@echo "make dots"
	@echo "sudo make scripts"

dots: tmux git bzr zsh
	@echo "you edit ~/.bazaar/locations.conf for bzr repository if necessary."
	@echo ""
	@echo " $$ cat ~/.bazaar/locations.conf"
	@echo " [/home/kiyoto/.emacs.d]"
	@echo " email = HAMANO Kiyoto <khiker.mail@gmail.com>"

scripts: udev-script

tmux:
	$(call symlink,.tmux.conf)
	$(call symlink,.tmux.conf.linux)
	$(call symlink,.tmux.conf.macos)

git:
	$(call symlink,.gitconfig)

bzr:
	$(call symlink,.bazaar)

zsh:
	$(call symlink,.zshrc)
	$(call symlink,.zlogin)

udev-script:
	install -m 0644 ./udev/00-my-init.rules      /etc/udev/rules.d
	install -m 0755 ./udev/udev_init.sh          /usr/local/bin
	install -m 0755 ./udev/udev_init_keyboard.sh /usr/local/bin
