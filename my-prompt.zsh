#! /bin/zsh

# [How to use]
#
# add following lines to your .zshrc
#
#   source "/path/to/my-prompt.zsh"
#   typeset -Uga precmd_functions
#   precmd_functions+=my-prompt
#
# [Note]
#
# this script uses following external commands.
#
# * sed
# * awk
# * wc
# * hostname
# * md5sum
# * cut
# * tr
# * git
#
# and this script see following file to get load average.
#
# * /proc/loadavg


function _my-prompt--loadavg-get1() {
    [[ -x `whence -p awk`    ]] || return 1;
    [[ -f /proc/loadavg      ]] || return 1;

    echo "$(cat /proc/loadavg | awk '{printf "%s %s %s", $1, $2, $3}')"
}

function _my-prompt--loadavg-get() {
    local count=0
    local string=''
    local avg

    # change text colors by the number of load average.
    #
    # * load average <=  3 ... green
    # * load average <=  6 ... yellow
    # * load average <= 10 ... magenta
    # * load average >  10 ... red
    for avg in `_my-prompt--loadavg-get1`; do
        [[ ! "$count" = "0" ]] && string+=" "

        case $avg in
        [012].*)  string+="%{%F{green}%}${avg}%f"   ;;
        [345].*)  string+="%{%F{yellow}%}${avg}%f"  ;;
        [6789].*) string+="%{%F{magenta}%}${avg}%f" ;;
        *)        string+="%{%F{red}%}${avg}%f"     ;;
        esac

        count=$(( count + 1 ))
    done

    echo -n $string
}

function _my-prompt--hostname() {
    [[ -x `whence -p hostname` ]] || return 1;
    [[ -x `whence -p md5sum`   ]] || return 1;
    [[ -x `whence -p cut`      ]] || return 1;

    # change hostname text color from hostname. change the hostname text
    # color to make it clear the hosts that oneself logs in (to avoid
    # miss-operation).
    local digit="$(hostname | md5sum | cut -c1-2)"

    # printf is zsh built-in.
    printf "%d" 0x${digit}
}

function _my-prompt--git-prompt-stash-count {
    [[ -x `whence -p git` ]] || return ;
    [[ -x `whence -p wc`  ]] || return ;
    [[ -x `whence -p tr`  ]] || return ;

    # display whether the stash exists or not to avoid to forget the
    # stash pop.
    local count="$(git stash list 2>/dev/null | wc -l | tr -d ' ')"
    [[ "$count" -gt 0 ]] && echo " ($count)"
}

function _my-prompt--count() {
    [[ -x `whence -p wc`  ]] || return 1;
    [[ -x `whence -p sed` ]] || return 1;

    # length of the specified text excluding the shell escape sequence.
    print -n -P $1 | sed -e $'s/\e\[[0-9;]*m//g' | wc -m | sed -e 's/ //g'
}

function _my-prompt--get-left() {
    local left=''
    local name="$(_my-prompt--hostname || echo '')"

    left+='%{%F{red}%}-%f'
    left+='%{%F{green}%}%n%f'
    left+='%{%F{red}%}@%f'
    left+="%{%B%U%}%{\e[38;5;${name}m%}%m%b%u"
    left+='%{%F{red}%}:%f'
    left+='%{%F{cyan}%}(%(5~,%-2~/.../%2~,%~))%f'

    echo -n $left
}

function _my-prompt--get-right() {
    local right=''
    local avg="$(_my-prompt--loadavg-get || echo '')"

    if [[ -n "${avg}" ]]; then
        right+='%{%F{green}%}|%f'
        right+="${avg}"
        right+='%{%F{green}%}|%f'
    fi

    right+='%{%F{red}%}-%f'
    right+='%{%F{red}%}-%f'
    right+='[%{%B%}%D{%Y/%m/%d %H:%M}%{%b%}]'
    right+='%{%F{red}%}-%f'

    echo -n $right
}

function _my-prompt--get-seprator() {
    local lnum="$(_my-prompt--count $1 || echo ${COLUMNS})"
    local rnum="$(_my-prompt--count $2 || echo 0)"
    local chrs="$((COLUMNS - lnum - rnum))"

    echo -n "%{%F{red}%}${(l:${chrs}::-:)}%f"
}

function _my-prompt() {
    local lstr="$(_my-prompt--get-left)"
    local rstr="$(_my-prompt--get-right)"
    local sstr="$(_my-prompt--get-seprator $lstr $rstr)"

    local string=''

    string+='%{%(?..%K{magenta})%}'
    string+="${lstr}"
    string+="${sstr}"
    string+="${rstr}"
    string+='%{%(?..%k)%}'
    string+="\n"
    string+='%{%F{red}%}-%f'
    string+='[%h]'
    string+='%{%F{blue}%}%(1j.(%j).)%f'
    string+='%{%B%}%(!.%{%F{red}%}.%{%F{green}%})%(!.#.$) %f%b'
    string+='%{${reset_color}%}'

    echo -n "${string}"
}

function my-prompt() {
    PROMPT="$(_my-prompt)"

    # right prompt settings. diplay vcs related informations.
    {
        RPROMPT=""

        psvar=()

        LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 vcs_info
        if [[ -n "$vcs_info_msg_0_" ]]; then
            RPROMPT+="%{%F{green}%}$vcs_info_msg_0_%{$reset_color%}"
            RPROMPT+="$(_my-prompt--git-prompt-stash-count)"
        fi
    }
}

# my-prompt.zsh ends here.
