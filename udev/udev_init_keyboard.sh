#! /bin/bash

function main() {
    DISPLAY=":0.0"
    HOME=/home/kiyoto/
    XAUTHORITY=$HOME/.Xauthority
    export DISPLAY XAUTHORITY HOME

    xset r rate 200 60
}

main
