#! /bin/zsh


function my-keyrepeat() {
    [[ -x `whence -p xset` ]] && xset r rate 200 60
}

my-keyrepeat

# .zlogin ends here
