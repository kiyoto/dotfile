#! /bin/sh

# [How to Use]
#
# add this line to your *rc file.
#
# LESSOPEN="| /path/to/lesspipej.sh %s"
#
# [Note]
#
# from http://www.nets.ne.jp/~matsu/linuxtips/

CONV_UTF8_CMD_PATH="$(which nkf)"
CONV_UTF8_CMD_OPTS="-w -Lu"

convert_to_utf8() {
	FILE_PATH="$1"

	[ -z "${FILE_PATH}" ] && return 1

	${CONV_UTF8_CMD_PATH} ${CONV_UTF8_CMD_OPTS} "${FILE_PATH}"
}

config_lesspipe() {
	LESSPIPE_PATH=""

	# for lesspipe.sh
	LESSPIPE_DEBIAN="/usr/bin/lesspipe"
	LESSPIPE_REDHAT="/usr/bin/lesspipe.sh"
	[ -e "${LESSPIPE_DEBIAN}" ] && LESSPIPE_PATH="${LESSPIPE_DEBIAN}"
	[ -e "${LESSPIPE_REDHAT}" ] && LESSPIPE_PATH="${LESSPIPE_REDHAT}"

	# for source-highlight
	LESSPIPE_HLIGHT1="/usr/share/source-highlight/src-hilite-lesspipe.sh"
	LESSPIPE_HLIGHT2="/usr/bin/src-hilite-lesspipe.sh"
	[ -e "${LESSPIPE_HLIGHT1}" ] && LESSPIPE_PATH="${LESSPIPE_HLIGHT1}"
	[ -e "${LESSPIPE_HLIGHT2}" ] && LESSPIPE_PATH="${LESSPIPE_HLIGHT2}"

	echo "${LESSPIPE_PATH}"
}

run_with_convert_to_utf8() {
	ARGS_LEN="$#"
	ARGS_FILE="$1"

	if [ "${ARGS_LEN}" -eq "0" ] || [ "${ARGS_FILE}" = "-" ] ; then
		convert_to_utf8 "$ARGS_FILE"
		return $?
	fi

	LESSPIPE_PATH="$(config_lesspipe)"
	if [ -x "${LESSPIPE_PATH}" ]; then
		${LESSPIPE_PATH} "${ARGS_FILE}" | convert_to_utf8 - 2> /dev/null
		return $?
	fi

	convert_to_utf8 "${ARGS_FILE}" 2> /dev/null
}

run() {
	ARGS_FILE="$1"

	LESSPIPE_PATH="$(config_lesspipe)"
	if [ -x "${LESSPIPE_PATH}" ]; then
		${LESSPIPE_PATH} "${ARGS_FILE}" 2> /dev/null
		return $?
	fi

	cat "${ARGS_FILE}" 2> /dev/null
}

main() {
	ARGS="$@"
	ARGS_FILE="$1"

	if [ -x "${CONV_UTF8_CMD_PATH}" ] ; then
		run_with_convert_to_utf8 "${ARGS}"
		return $?
	fi

	run "${ARGS_FILE}"
}

main "$@"

# lesspipej.sh ends here.

