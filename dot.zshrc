#! /bin/zsh

OS=`uname -s`


# Internal funtions

function _command_exist() {
    local cmd="$1"

    if [[ ! -x `whence -p $cmd` ]]; then
        echo "E: command '${cmd}' is necessary." 1>&2
        return 1
    fi

    return 0
}

function _check_external_commands() {
    _command_exist "readlink" || return 1
    _command_exist "pkill"    || return 1
    _command_exist "tmux"     || return 1

    case $OS in
        Linux)
            _command_exist "find"         || return 1
            _command_exist "keychain"     || return 1
            _command_exist "hostname"     || return 1

            # also install
            #
            # * anyenv
            # * direnv
            # * xsel
            # * xset
            ;;
    esac

    return 0
}

function _init_env_bzr() {
    BZR_EMAIL="HAMANO Kiyoto <khiker.mail@gmail.com>"
    BZR_PAGER='more'

    export BZR_EMAIL BZR_PAGER
}

function _init_env_lscolor() {
    LSCOLORS=ExFxCxdxBxegedabagacad
    LS_COLORS='di=01;34:ln=01;35:so=01;32:ex=01;31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'

    export LSCOLORS LS_COLORS
}

function _init_env_pager() {
    PAGER='less'

    export PAGER
}

function _init_env_less() {
    local scriptdir="$(dirname `readlink $HOME/.zshrc`)"

    JLESSCHARSET='japanese'
    LESS='-R'
    LESSOPEN="| $scriptdir/lesspipej.sh %s"

    export JLESSCHARSET LESS LESSOPEN
}

function _init_env_gpg() {
    GPGKEY=5BF7C2E7

    export GPGKEY
}

function _init_env_java() {
    case $OS in
        Linux)
            local java_path=`readlink -f /usr/bin/java | sed "s:bin/java::"`
            if [[ -n "${java_path}" ]]; then
                JAVA_HOME="${java_path}"
                CLASSPATH=.:$JAVA_HOME/jre/lib:$JAVA_HOME/lib/tools.jar

                export JAVA_HOME CLASSPATH
            fi
            ;;
        MSYS_NT-10.0)
            :
            ;;
    esac
}

function _init_env_lang() {
    case $OS in
        Linux)
            LANG="en_US.UTF-8"
            LC_CTYPE="en_US.UTF-8"

            export LANG LC_CTYPE
            ;;
        Darwin)
            alias find='gfind'
            ;;
        MSYS_NT-10.0)
            LANG="ja_JP.UTF-8"
            LC_CTYPE="ja_JP.UTF-8"

            export LANG LC_CTYPE
            ;;
    esac
}

function _init_env_editor() {
    case $OS in
        Linux)
            EDITOR="emacsclient -s $HOME/.emacs.d/tmp/emacs`id -u`/server -nw"

            export EDITOR
            ;;
        MSYS_NT-10.0)
            EDITOR="vim"

            export EDITOR
            ;;
    esac
}

function _init_env() {
    path=( $path $HOME/local/sbin $HOME/local/bin )
    path=( $path /usr/local/sbin  /usr/local/bin )
    path=( $path $HOME/.local/bin )
    path=( $path /usr/sbin        /usr/bin )
    path=( $path /sbin            /bin )

    ld_library_path=( $ld_library_path $HOME/local/lib )
    ld_library_path=( $ld_library_path /usr/local/lib )

    fpath=( $fpath $HOME/local/share/zsh )

    case $OS in
        Linux)
            path=( $HOME/.poetry/bin $HOME/.anyenv/bin $HOME/.cargo/bin $path )

            # for xdvi ad-hoc countermeasure
            export XDVIINPUTS=/etc/texmf/xdvi
            export VTE_CJK_WIDTH=wide
            ;;
        MSYS_NT-10.0)
            :
            ;;
    esac

    export PATH LD_LIBRARY_PATH

    if [ ! "x${TERM}" = "xdumb" ]; then
        TERM=xterm-256color
        export TERM
    fi

    _init_env_bzr
    _init_env_lscolor
    _init_env_pager
    _init_env_less
    _init_env_gpg
    _init_env_java
    _init_env_lang
    _init_env_editor

}

function _init_zsh_features() {
    autoload -U colors
    autoload -U compinit
    autoload -Uz vcs_info

    colors
    compinit

    setopt auto_pushd       \
           pushd_silent     \
           nolistbeep       \
           ignoreeof        \
           auto_list        \
           auto_menu        \
           correct          \
           no_flow_control  \
           share_history    \
           hist_ignore_dups \
           hist_ignore_space \
           extended_history \
           zle              \
           auto_cd          \
           prompt_subst     \
           mark_dirs        \
           list_types

    if [ "x${TERM}" = "xdumb" ]; then
        unset zle_bracketed_paste
    fi

    # config for histories
    HISTFILE=$HOME/.zhistory
    HISTSIZE=100000
    SAVEHIST=100000

    # remove '/' from wordchars.
    WORDCHARS="*?_-.[]~=&;!#$%^(){}<>"

    # completing by emacs key bindings.
    zstyle ':completion:*:default' menu         select=1
    zstyle ':completion:*:default' menu         select          true
    zstyle ':completion:*'         matcher-list 'm:{a-z}={A-Z}'
    zstyle ':completion:*' \
           list-colors     \
           'di=;34;1' 'ln=;35;1' 'so=;32;1' 'ex=31;1' 'bd=46;34' 'cd=43;34'


    # show vcs information to the prompt.
    zstyle ':vcs_info:*'           formats       '(%s)-[%b]'
    zstyle ':vcs_info:*'           actionformats '(%s)-[%b|%a]'
    zstyle ':vcs_info:(svn|bzr):*' branchformat  '%b:r%r'
    zstyle ':vcs_info:bzr:*'       use-simple    true
}

function _init_prompt() {
    local scriptdir="$(dirname `readlink $HOME/.zshrc`)"

    case $OS in
        MSYS_NT-10.0)
            # my-prompt.sh is too slow under the MSYS environment. use
            # simple prompt.
            local str=""

            str+="%{$fg[red]%}%n%{$reset_color%}@%{$fg[green]%}%m "
            str+="%{$fg_no_bold[yellow]%}%1~%{$reset_color%}"
            str+="\n"
            str+="%(!.#.$) "

            PROMPT="$(echo -n ${str})"
            ;;
        *)
            case $TERM in
            "dumb")
                PROMPT="> "
                ;;
            *)
                source "$scriptdir/my-prompt.zsh"
                typeset -Uga precmd_functions
                precmd_functions+=my-prompt
                ;;
            esac
            ;;
    esac

    SPROMPT="%r？ べ、別にあんたのために修正したんじゃないんだからね！ [n,y,a,e]："
}

function _init_aliases() {
    alias ls='ls -F --show-control-chars --color=auto'
    alias re='pkill -USR1 -U `id -u` zsh; my-check-dotfile-updates'

    case $OS in
        Linux)
            alias eclient='emacsclient -s ~/.emacs.d/tmp/emacs`id -u`/server -c'
            ;;
        Darwin)
            alias ls='gls -F --show-control-chars --color=auto'
            alias eclient='emacsclient -s ~/.emacs.d/tmp/emacs`id -u`/server -c'
            ;;
        MSYS_NT-10.0)
            :
            ;;
    esac
}

function my_kill_ring_save() {
    zle copy-region-as-kill
    print -rn $CUTBUFFER | xsel -i
}

function my_kill_region() {
    if [ $REGION_ACTIVE -eq 0 ]; then
        zle backward-kill-word
    else
        zle kill-region
        print -rn $CUTBUFFER | xsel -i
    fi
}

function my_yank() {
    CUTBUFFER=$(xsel -o -p < /dev/null)
    zle yank
}

function _init_shortcuts() {
    [[ ! -x `whence -p xsel` ]] && return ;

    zle -N my_kill_ring_save
    zle -N my_kill_region
    zle -N my_yank

    bindkey -e '\eW' my_kill_ring_save
    bindkey -e '^W'  my_kill_region
    bindkey -e '^Y'  my_yank
}

function _init_keychains() {
    [[ "x$USER" = "xroot" ]] && return ;

    case $OS in
    Linux|Darwin)
        local regex=".*/id_\(rsa\|ecdsa\|ed25519\)\.[^.]*"
        local key
        for key in `find $HOME/.ssh -regex "${regex}"`; do
            keychain -q $key
        done
        source ~/.keychain/`hostname`-sh
        ;;
    MSYS_NT-10.0)
        # Msys2 currently does not support keychain.
        # see https://github.com/Alexpux/MSYS2-packages/issues/369
        ;;
    esac
}

function _init_reload() {
    source $HOME/.zshrc

    if tmux show-environment | grep -q '^DISPLAY'; then
        export $(tmux show-environment | grep '^DISPLAY')
    fi
}

function _init_xenv() {
    [[ -x `whence -p direnv` ]] && eval "$(direnv hook zsh)"
    [[ -x `whence -p anyenv` ]] && eval "$(anyenv init -)"
    [[ -f "~/.poetry/env"    ]] && source "~/.poetry/env"
}

# Utilitie functions

function my-check-dotfile-updates() {
    local scriptdir="$(dirname `readlink $HOME/.zshrc`)"

    local chash="$(cd ${scriptdir}; git rev-parse origin/main)"
    local phash="$(cd ${scriptdir}; git ls-remote $(git remote get-url origin) HEAD | awk '{print $1}')"

    [[ -z chash ]] && return 1
    [[ -z phash ]] && return 1

    if [[ ! "x${phash}" = "x${chash}" ]]; then
        echo "SCRIPT UPGRADABLE: parent revno is ${phash}. current is ${chash}."
    fi
    return 0
}

# Main

if _check_external_commands; then
    _init_zsh_features

    # it looks like these typeset should be defined in the top level.
    # (it does not work when these are defined within function)
    typeset -T LD_LIBRARY_PATH ld_library_path
    typeset -U path PATH cdpath fpath manpath ld_library_path

    _init_env
    _init_prompt
    _init_aliases
    _init_shortcuts
    _init_keychains
    _init_xenv

    trap "_init_reload" USR1

    [[ -f ~/.zlocal ]] && source ~/.zlocal
fi

# .zshrc ends here
